﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace lessons
{
    class Program
    {
        static void Main(string[] args)
        {
            //connection
            string conString = @"Data Source = MSI; Initial Catalog=arbolishvili; Integrated Security=True";
            SqlConnection conn = new SqlConnection(conString);
            conn.Open();

            //update 
            string updateQuery = "UPDATE credit SET amount = @newAmount WHERE id=1";
            SqlCommand cmd2 = new SqlCommand(updateQuery, conn);

            cmd2.Parameters.AddWithValue("@newAmount", 4);

            int rowsAffected = cmd2.ExecuteNonQuery();
            Console.WriteLine($"Rows updated: {rowsAffected}");

            //select 
            string queryString = "Select * from credit";
            SqlCommand cmd = new SqlCommand(queryString, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                Console.WriteLine(reader[0].ToString() + ":" + reader[1].ToString());
            }

            Console.ReadKey();

            //Create
            //string queryCreate = "CREATE TABLE [dbo].[Test](\r\n\t[test_id] [int] NOT NULL,\r\n\t[title] [varchar](50) NULL,\r\n CONSTRAINT [PK_Test] PRIMARY KEY CLUSTERED \r\n(\r\n\t[test_id] ASC\r\n)) ON [PRIMARY]";
            //using (SqlCommand cmd1 = new SqlCommand(queryCreate, conn))
            //{
            //    int res = cmd1.ExecuteNonQuery();
            //}

            string countQuery = "Select count(id) from school";
            using (SqlCommand cmd1 = new SqlCommand(countQuery, conn))
            {
                int count = (int)cmd1.ExecuteScalar();
                Console.WriteLine(count);
            }

            //Insert
            string insertQuery = "INSERT INTO [dbo].[school]([school]) VALUES (@school)";
            using (SqlCommand cmd1 = new SqlCommand(insertQuery, conn))
            {
                cmd1.Parameters.Add("@school", SqlDbType.VarChar);               
                cmd1.Parameters["@school"].Value = "marketing";
        
                cmd1.ExecuteNonQuery();
            }

            //Delete
            string deleteQuery = "DELETE from school where id=2";
            using (SqlCommand cmd1 = new SqlCommand(deleteQuery, conn))
            {
                cmd1.ExecuteNonQuery();
            }

            Console.ReadKey();

            conn.Close();
        }
    }
}
